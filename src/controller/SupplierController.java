package controller;

import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JTextField;

import model.Supplier;
import model.VideogameCenter;

/**
 * @author Mattia Ferrari
 */

public interface SupplierController {

	/**
	 * this method insert new Supplier
	 * 
	 * @param name
	 * @param codeCarrel
	 * @return 
	 * 
	 * @return String (String name, int codeCarrel)
	 */
	 void insertSupplier(String name, int codeCarrel);

	/**
	 * this method return the videogamecenter
	 * 
	 * @return
	 */
	VideogameCenter getCenter();

	/**
	 * this method modify the supplier
	 * 
	 * @param name
	 * @param code
	 * @param displayNameSupplier
	 * 
	 * @return String (String name, int code, JLabel displayNameSupplier)
	 */
	String modifySupplier(String name, int code,JLabel displayNameSupplier);

	/**
	 * this method check the name of all Supplier
	 * 
	 * @param name
	 * 
	 * @return boolean(String name);
	 */
	boolean checkName(String name);

	/**
	 * this method check the code of all Supplier
	 * 
	 * @param code
	 * 
	 * @return boolean(int code)
	 */
	boolean checkCode(int code);

	/**
	 * exit from Supplier
	 */
	void quit();
	
	/**
	 * exit from InsertSupplier
	 */
	void quitInsert();

	/**
	 * exit from ModifySupplier
	 */
	void quitModify();
	/**
	 * exit from ChoiceSupplier
	 */
	void quitChoice();
	
	/**
	 * get textField for selected choice
	 */
	JTextField getChoiceTextField();
	
	/** 
	 * @return
	 */
	public ArrayList<Supplier> getListSupplierView();
	
	void deleteSupplier(Supplier supplier);
	
	void setModifySupplierController(Supplier supplier, JLabel nameSupplier);
}
