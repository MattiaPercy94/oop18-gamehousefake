package controller;



import model.VideogameCenter;
import view.GameSectionViewImpl;

import view.InsertSectionImpl;
import view.InsertSupplierViewImpl;
import view.InsertGameViewImpl;

import view.MainPanelImpl;
import view.SupplierViewImpl;

/**
 * @author Mattia Ferrari
 */

public class MainControllerImpl implements MainController {

	private VideogameCenter modelCenter;
	private MainPanelImpl panel;

	public MainControllerImpl(VideogameCenter modelCenter, MainPanelImpl panel) {

		this.modelCenter = modelCenter;
		this.panel = panel;

	}

	public void insertSectionView() {

		InsertSectionImpl gamePanel = new InsertSectionImpl();
		SectionController sectionController = new SectionControllerImpl(this.modelCenter, panel, gamePanel);

		panel.setVisible(false);
		gamePanel.setEnabled(true);

		gamePanel.addObserver(sectionController);

	}
	
	/***
	 * insert supplier from MainController
	 */
	public void insertSupplierView() {
		InsertSupplierViewImpl insertSupplier = new InsertSupplierViewImpl();
		SupplierController supplierController = 
				new SupplierControllerImpl(this.modelCenter, panel, insertSupplier);

		panel.setVisible(false);
		insertSupplier.setEnabled(true);

		insertSupplier.addObserver(supplierController);

		
	}

	public void insertGameView() {

		InsertGameViewImpl insertGamePanel = new InsertGameViewImpl(panel);
		GameController insertGameController = new GameControllerImpl(this.modelCenter, panel,
				insertGamePanel);

		this.panel.setVisible(false);
		insertGamePanel.setEnabled(true);

		insertGamePanel.addObserver(insertGameController);

	}

	public void GameSection() {

		GameSectionViewImpl View = new GameSectionViewImpl();
		GameSectionController Controller = new GameSectionControllerImpl(this.modelCenter,
				panel, View);

		this.panel.setVisible(false);
		View.setEnabled(true);

		View.addObserver(Controller);

	}
	
	public void Supplier() {

		SupplierViewImpl View = new SupplierViewImpl();
		SupplierController Controller = new SupplierControllerImpl(
				this.modelCenter,this.panel,View);

		this.panel.setVisible(false);
		View.setEnabled(true);

		View.addObserver(Controller);

	}
	
	

	public boolean logIn(String username, String password) {

		return this.modelCenter.logIn(username, password);

	}
	
	public void setFile(){
		
		this.modelCenter.insertSectionFile();
	}

	public VideogameCenter getVideogameCenter() {

		return this.modelCenter;

	}

	

	
	
		
}
