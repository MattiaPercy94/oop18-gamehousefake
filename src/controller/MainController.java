package controller;


/**
 * @author Mattia Ferrari
 */

public interface MainController {

	/**
	 * this method create insertSectionView
	 */
	void insertSectionView();
	
	/**
	 * this method create insertSupplierView
	 */
	void insertSupplierView();

	/**
	 * this method create insertGameView
	 */
	void insertGameView();

	/**
	 * this method create the GameSectionView
	 */
	public void GameSection();
	
	/**
	 * this method create the SupplierView
	 */
	public void Supplier();
	
	/**
	 * this method create loginView
	 * 
	 * @param username
	 * @param password
	 * 
	 * @return boolean(String username, String password)
	 */
	boolean logIn(String username, String password);
	
	
	/**this method update the file on close of application
	 * 
	 */
	public void setFile();
	
	

}
