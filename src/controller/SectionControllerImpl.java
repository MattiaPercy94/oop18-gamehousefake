package controller;

import model.Section;
import model.SectionImpl;
import model.VideogameCenter;

import view.GameSectionViewImpl;

import view.InsertSectionImpl;

import view.MainPanelImpl;

import view.ModifySectionViewImpl;

import javax.swing.JLabel;

/**
 * @author Mattia Ferrari
 */


public class SectionControllerImpl implements SectionController {
	
	private VideogameCenter modelCenter;
	private MainPanelImpl precPanel;
	private InsertSectionImpl nextPanel;
	private GameSectionViewImpl directPanel;
	private ModifySectionViewImpl modifyView;

	public SectionControllerImpl(VideogameCenter modelCenter, MainPanelImpl precPanel, InsertSectionImpl nextPanel) {

		this.modelCenter = modelCenter;
		this.precPanel = precPanel;
		this.nextPanel = nextPanel;

	}

	public SectionControllerImpl(VideogameCenter modelCenter, GameSectionViewImpl directPanel,
			ModifySectionViewImpl modify) {

		this.modelCenter = modelCenter;
		this.directPanel = directPanel;
		this.modifyView = modify;

	}

	public void insertSection(String name, int max, int codeCarrel) {

		
		Section newSection = new SectionImpl(name, max, codeCarrel);

		modelCenter.addSection(newSection);


	}

	public String modifySection(String name, int max, int code, JLabel displayNameSection) {

		String check = null;

		if (!String.valueOf(max).matches("[+]?\\d*\\.?\\d+")) {

			check = Utility.Utility.ERRORQUANTITY;
			directPanel.setPanel(this.modelCenter.getListSection());
			return check;

		} else {

			for (int i = 0; i < modelCenter.getListSection().size(); i++) {
				
				if(modelCenter.getListSection().get(i).quantityTotal() > max){
					
					
					 check = Utility.Utility.ERRORMODIFYQUANTIY;
					 directPanel.setPanel(this.modelCenter.getListSection());
					
					return check;
				}

				if (modelCenter.getListSection().get(i).getCodeSection() == code) {

					if (checkName(name) == true) {

						check = Utility.Utility.CHECKQUANTITY;

						modelCenter.getListSection().get(i).setMaxGameSection(max);
						directPanel.setPanel(this.modelCenter.getListSection());
							
						
						return check;

					} else {

						modelCenter.getListSection().get(i).setName(name);
						modelCenter.getListSection().get(i).setMaxGameSection(max);

						 directPanel.setPanel(this.modelCenter.getListSection());
						

						check = Utility.Utility.SUCCESSMODIFY;

					}
				}

			}
		}


		return check;


	}

	public boolean checkName(String name) {

		for (int i = 0; i < modelCenter.getListSection().size(); i++) {

			if (modelCenter.getListSection().get(i).getName().equals(name)) {

				return true;
			}

		}

		return false;
	}

	public boolean checkCode(int code) {

		for (int i = 0; i < modelCenter.getListSection().size(); i++) {

			if (modelCenter.getListSection().get(i).getCodeSection() == code) {

				return true;

			}

		}

		return false;

	}
	

	public VideogameCenter getCenter() {

		return this.modelCenter;
	}

	public void quit() {

		
		this.precPanel.setVisible(true);
		this.nextPanel.setVisible(false);

	}

	public void quitModify() {

		this.directPanel.setVisible(true);
		this.modifyView.setVisible(false);

	}
	

}
