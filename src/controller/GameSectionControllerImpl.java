package controller;

import view.GameSectionViewImpl;

import view.MainPanelImpl;

import view.ModifySectionViewImpl;
import view.ManagementGameViewImpl;

import java.util.ArrayList;

import javax.swing.JLabel;

import model.Section;

import model.VideogameCenter;

/**
 * @author Mattia Ferrari
 */

public class GameSectionControllerImpl implements GameSectionController {

	private VideogameCenter modelCenter;
	private MainPanelImpl panel;
	private GameSectionViewImpl currentPanel;

	public GameSectionControllerImpl(VideogameCenter modelCenter, MainPanelImpl panel,
			GameSectionViewImpl currentPanel) {

		this.modelCenter = modelCenter;
		this.panel = panel;
		this.currentPanel = currentPanel;
	}

	public void deleteSection(Section section) {

				modelCenter.getListSection().remove(section);
				currentPanel.setPanel(getListSectionView());
	}

	public void setModifyController(Section section, JLabel nameSection) {

		ModifySectionViewImpl sectionPanel = new ModifySectionViewImpl();
		SectionController sectionController = new SectionControllerImpl(this.modelCenter, currentPanel,
				sectionPanel);

		sectionPanel.setData(section.getName(), section.getMaxGameSection(),
				section.getCodeSection(), nameSection);

		currentPanel.setVisible(false);
		sectionPanel.setVisible(true);

		sectionPanel.addObserver(sectionController);

	}

	public void setModifyGameController(Section section) {

		ManagementGameViewImpl gamePanel = new ManagementGameViewImpl();
		GameController gameController = new GameControllerImpl(this.modelCenter,section, currentPanel, gamePanel);

		gamePanel.listGame(section.getListGame());

		currentPanel.setVisible(false);
		gamePanel.setVisible(true);

		gamePanel.addObserver(gameController);

	}
	
	public ArrayList<Section> getListSectionView(){
		
		return modelCenter.getListSection();
	}


	public void quit() {

		this.panel.setVisible(true);
		this.currentPanel.setVisible(false);

	}

}
