package controller;

import view.ModifyGameImpl;

import view.ManagementGameViewImpl;

import model.Section;

import javax.swing.JFrame;
import javax.swing.JTextField;

import model.Game;
import model.VideogameCenter;


/**
 * @author Mattia Ferrari
 */

public interface GameController {

	/**
	 * this method return the videogamecenter
	 * 
	 * @return
	 */
	VideogameCenter getCenter();

	/**
	 * this method check the game after insert
	 * 
	 * @param section
	 * @param name
	 * @param code
	 * @param index
	 * @param price
	 * @param quantity
	 * 
	 * @return String(Section section, String name, int code, int
	 *         index, int price, int quantity, String supplier )
	 */
	public String insertGame(
			Section section, String name, int code, int index, 
			int price, int quantity, String supplier ) ;

	/**
	 * this method create the ModifyGameView
	 * 
	 * @param game
	 * @param view
	 * @param viewNameGame
	 * @param viewQuantityGame
	 * @param viewPriceGame
	 */
	public void modifyController(Game game, ManagementGameViewImpl view);

	/**
	 * this method check the modify of game
	 * 
	 * @param code
	 * @param name
	 * @param price
	 * @param quantity
	 * @param viewNameGame
	 * @param viewQuantityGame
	 * @param viewPriceGame
	 * @param supplier
	 * @return String(int code, String name, int price, int quantity, JLabel
	 *         viewNameGame, JLabel viewQuantityGame, JLabel
	 *         viewPriceGame)
	 */
	public String changeGame(
			int code, String name, int price, int quantity,String supplier);

	/**
	 * this method check the quantity of section
	 * 
	 * @param section
	 * @param quantity
	 * 
	 * @return boolean((Section section, int quantity)
	 */
	boolean checkQuantity(Section section,int code, int quantity);

	/**
	 * this method check the name of game
	 * 
	 * @param name
	 * 
	 * @return boolean(String name)
	 */
	public boolean checkName(String name);

	/**
	 * this method check the code of game
	 * 
	 * @param code
	 * 
	 * @return boolean(int code)
	 */
	public boolean checkCode(int code);

	/**
	 * this method delete a game
	 * 
	 * @param game
	 * @param quantityGame
	 */
	public void deleteGame(Game game);

	/**
	 * exit
	 */
	void quit();

	/**
	 * exit from modify
	 */
	void quitModify();

	/**
	 * exit from ModifyGameView
	 * 
	 * @param view
	 */
	public void quitModifyGame(ModifyGameImpl view);
	
	/**
	 * for Choice Supplier
	 * */
	public void ChoiceSupplierView(JTextField choiceTextField ,JFrame choiceFrame);

}
