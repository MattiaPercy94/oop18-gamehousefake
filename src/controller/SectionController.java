package controller;

import javax.swing.JLabel;
import model.VideogameCenter;

/**
 * @author Mattia Ferrari
 */

public interface SectionController {

	/**
	 * this method insert new Section
	 * 
	 * @param name
	 * @param codeCarrel
	 * @param max
	 * @return 
	 * 
	 * @return String (String name, int codeCarrel, int max)
	 */
	 void insertSection(String name, int max, int codeCarrel);

	/**
	 * this method return the videogamecenter
	 * 
	 * @return
	 */
	VideogameCenter getCenter();

	/**
	 * this method modify the section
	 * 
	 * @param name
	 * @param max
	 * @param code
	 * @param displayNameSection
	 * 
	 * @return String (String name, int max, int code, JLabel
	 *         displayNameSection)
	 */
	String modifySection(String name, int max, int code,
			JLabel displayNameSection);

	/**
	 * this method check the name of all Section
	 * 
	 * @param name
	 * 
	 * @return boolean(String name);
	 */
	boolean checkName(String name);

	/**
	 * this method check the code of all Section
	 * 
	 * @param code
	 * 
	 * @return boolean(int code)
	 */
	boolean checkCode(int code);

	/**
	 * exit from InsertSection
	 */
	void quit();

	/**
	 * exit from ModifySection
	 */
	void quitModify();
}
