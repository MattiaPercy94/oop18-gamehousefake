package controller;

import view.ChoiceSupplierViewImpl;
import view.GameSectionViewImpl;

import view.InsertGameViewImpl;

import view.MainPanelImpl;

import view.ModifyGame;
import view.ModifyGameImpl;
import view.ManagementGameViewImpl;
import model.Section;

import javax.swing.JFrame;
import javax.swing.JTextField;

import model.Game;
import model.VideogameCenter;
import model.GameImpl;
/**
 * @author Mattia Ferrari
 */

public class GameControllerImpl implements GameController {

	private VideogameCenter modelCenter;
	private Section section; 
	private MainPanelImpl panel;
	private InsertGameViewImpl nextPanel;
	private GameSectionViewImpl directPanel;
	private ManagementGameViewImpl modifyView;

	public GameControllerImpl(VideogameCenter modelCenter, MainPanelImpl precPanel, InsertGameViewImpl nextPanel) {

		this.modelCenter = modelCenter;
		this.panel = precPanel;
		this.nextPanel = nextPanel;

	}

	public GameControllerImpl(VideogameCenter modelCenter, Section section,
			GameSectionViewImpl precPanel, ManagementGameViewImpl nextPanel) {

		this.modelCenter = modelCenter;
		this.directPanel = precPanel;
		this.modifyView = nextPanel;
		this.section = section;
	}

	public VideogameCenter getCenter() {

		return this.modelCenter;

	}

	public String insertGame(
			Section section, String name, int code, int index, 
			int price, int quantity, String supplier ) {

		if (checkQuantity(section, code, quantity) == false) {

			return Utility.Utility.ERRORCAPACITY;

		} else {

			if (checkCode(code) == true) {

				return Utility.Utility.ERRORCODE;

			}

			Game newGame = new GameImpl(name, code, price, quantity,supplier);

			modelCenter.getListSection().get(index).insertGame(newGame);
			modelCenter.insertSectionFile();
			
			return Utility.Utility.SUCCESSINSERT;

		}
	}

	public void modifyController(Game game, ManagementGameViewImpl view) {

		ModifyGame gamePanel = new ModifyGameImpl(view);
		GameController gameController = new GameControllerImpl(this.modelCenter, this.section,
				this.directPanel, this.modifyView);

		this.modifyView.setVisible(false);

		gamePanel.setData(game.getCodeGame(), game.getName(), game.getPrice(), game.getQuantity(),game.getSupplier());

		gamePanel.addObserver(gameController);

	}

	public String changeGame(
			int code, String name, int price, int quantity,String supplier) {

		String check;
		System.out.println(code+" "+name+" "+price+" "+quantity);

		for (int i = 0; i < modelCenter.getListSection().size(); i++) {

			for (int j = 0; j < modelCenter.getListSection().get(i).getListGame().size(); j++) {

				if (modelCenter.getListSection().get(i).getListGame().get(j).getCodeGame() == code) {

					if (quantity == 0) {

						modelCenter.getListSection().get(i)
								.deleteGame(modelCenter.getListSection().get(i).getListGame().get(j));

						check = Utility.Utility.CHECKDELETEGAME;

						modelCenter.insertSectionFile();

						return check;
					}

					check = Utility.Utility.CHECKQUANTITY;
					modelCenter.getListSection().get(i).getListGame().get(j).setPrice(price);
					modelCenter.getListSection().get(i).getListGame().get(j).setSupplier(supplier);
					modelCenter.insertSectionFile();

					if (checkQuantity(modelCenter.getListSection().get(i), code, quantity) == false) {

						check = Utility.Utility.ERRORCAPACITY;
						return check;

					} else {

						modelCenter.getListSection().get(i).getListGame().get(j).setQuantity(quantity);

						modelCenter.insertSectionFile();

					}

					if (checkName(name) == true) {

						check = Utility.Utility.CHECKGAME;

						return check;

					} else {

						modelCenter.getListSection().get(i).getListGame().get(j).setName(name);

						check = Utility.Utility.SUCCESSMODIFY;
					}

				}

			}

		}

		check = Utility.Utility.SUCCESSMODIFY;

		
		return check;

	}

	public void deleteGame(Game game) {

		if (game.getQuantity() == 0) {

			section.deleteGame(game);
			modifyView.setPanel(section.getListGame());

		} else {

			game.setQuantity(game.getQuantity() - 1);
			modifyView.setPanel(section.getListGame());

		}

	}

	public boolean checkQuantity(Section section, int code, int quantity) {

		int count = 0;
		
		System.out.println(section.getName()+" "+quantity);

		System.out.println(section.getListGame().size());

		for (int i = 0; i < section.getListGame().size(); i++) {
			if(section.getListGame().get(i).getCodeGame() != code)
				count = count + section.getListGame().get(i).getQuantity();
		}
		count = count + quantity;
		//count = section.getMaxGameSection() - count;

		if (section.getMaxGameSection() < count) {

			return false;

		} else {

			return true;
		}

	}

	public boolean checkName(String name) {

		for (int i = 0; i < modelCenter.getListSection().size(); i++) {

			for (int j = 0; j < modelCenter.getListSection().get(i).getListGame().size(); j++) {

				if (modelCenter.getListSection().get(i).getListGame().get(j).getName().equals(name)) {

					return true;
				}

			}

		}

		return false;
	}

	public boolean checkCode(int code) {

		for (int i = 0; i < modelCenter.getListSection().size(); i++) {

			for (int j = 0; j < modelCenter.getListSection().get(i).getListGame().size(); j++) {

				if (modelCenter.getListSection().get(i).getListGame().get(j).getCodeGame() == code) {

					return true;
				}

			}

		}

		return false;

	}
	public void quit() {

		this.panel.setVisible(true);
		this.nextPanel.setVisible(false);

	}

	public void quitModify() {

		this.directPanel.setVisible(true);
		this.modifyView.setVisible(false);

	}

	public void quitModifyGame(ModifyGameImpl view) {

		modifyView.setVisible(true);
		modifyView.setPanel(section.getListGame());
		view.setVisible(false);
	}
	
	@Override
	public void ChoiceSupplierView(JTextField choiceTextField, JFrame choiceFrame) {
		ChoiceSupplierViewImpl View = new ChoiceSupplierViewImpl();
		SupplierController Controller = 
				new SupplierControllerImpl(
						this.modelCenter,choiceTextField,choiceFrame,View
						);

		//this.panel.setVisible(false);
		choiceFrame.setVisible(false);
		View.setEnabled(true);

		View.addObserver(Controller);
		
	}

}
