package view;

import javax.swing.*;

import Utility.Utility;
import model.Section;
import controller.GameSectionController;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

/**
 * @author Mattia Ferrari
 */
public class GameSectionViewImpl extends JFrame implements
		GameSectionView {

	private static final long serialVersionUID = 1L;
	GameSectionController controller;
	private final JPanel sectionPanel = new JPanel(
			new GridLayout(0, 4, 2, 2));
	private final JLabel displayDelete = new JLabel("Elimina");
	private final JLabel displayModify = new JLabel("Modifica");
	private final JLabel displayGame = new JLabel("Gestione Giochi");
	private final JLabel dislayName = new JLabel("Nome Sezione");
	
	
	public GameSectionViewImpl() {

		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		this.setVisible(true);
		this.setResizable(false);

		this.setSize(700, 500);
		this.setBounds(300, 175, this.getWidth(), this.getHeight());

		this.setLayout(new BorderLayout());

		this.add(sectionPanel, BorderLayout.NORTH);

		this.setLocationRelativeTo(null);
		sectionPanel.add(dislayName);
		sectionPanel.add(displayModify);
		sectionPanel.add(displayGame);
		sectionPanel.add(displayDelete);

		sectionPanel.setBackground(new Color(192, 192, 192));

		JScrollPane scroll = new JScrollPane(sectionPanel,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		this.add(scroll);

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quit();

			}

		});

	}

	public void addObserver(GameSectionController controller) {

		this.controller = controller;

		listSection(controller.getListSectionView());
	}

	public void listSection(ArrayList<Section> section) {

		for (int i = 0; i < section.size(); i++) {

			JLabel newLabel = new JLabel(section.get(i).getName());

			JButton modifyButton = new JButton("Modifica Sezione");
			JButton deleteSection = new JButton("Elimina");
			JButton directGame = new JButton("Gestione Giochi");

			sectionPanel.add(newLabel);
			sectionPanel.add(modifyButton);
			sectionPanel.add(directGame);
			sectionPanel.add(deleteSection);

			actionEvent(deleteSection, section.get(i), modifyButton,
					newLabel, directGame, this);

			validate();

		}

	}

	public void actionEvent(JButton deleteSection, Section section,
			JButton modifySection, JLabel nameSection,
			JButton directGame, GameSectionViewImpl view) {

		deleteSection.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				final int exit = JOptionPane.showConfirmDialog(view,
						Utility.QUESTIONDELETE, "Cancellazione",
						JOptionPane.YES_NO_OPTION);
				if (exit == JOptionPane.YES_OPTION) {

					controller.deleteSection(section);

				}

			}

		});

		modifySection.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.setModifyController(section, nameSection);

			}

		});

		directGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.setModifyGameController(section);

			}

		});

	}

	public void setPanel(ArrayList<Section> section) {

		sectionPanel.removeAll();
		sectionPanel.add(dislayName);
		sectionPanel.add(displayModify);
		sectionPanel.add(displayGame);
		sectionPanel.add(displayDelete);
		validate();
		listSection(section);

	}

}
