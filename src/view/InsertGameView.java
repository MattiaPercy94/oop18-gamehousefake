package view;

import java.util.ArrayList;

import javax.swing.JButton;

import controller.GameController;
import model.Section;


/**
 * @author Mattia Ferrari
 */

public interface InsertGameView {

	/**
	 * this method works with the components of JFrame
	 * 
	 * @param index
	 * @param newButton
	 * @param section
	 */
	public void actionEvent(int index, JButton newButton,
			ArrayList<Section> section);

	/**
	 * this method give the list of Section
	 * 
	 * @param section
	 */
	public void listSection(ArrayList<Section> section);

	/**
	 * this method use the pattern Observer
	 * 
	 * @param controller
	 */
	public void addObserver(GameController controller);

}
