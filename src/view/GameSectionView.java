package view;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import controller.GameSectionController;
import model.Section;


/**
 * @author Mattia Ferrari
 */

public interface GameSectionView {

	/**
	 * this method works with the component of JFrame
	 * 
	 * @param newButton
	 * @param section
	 * @param modifySection
	 * @param nameSection
	 * @param directGame
	 */
	public void actionEvent(JButton deleteSection, Section section, JButton modifySection,
			JLabel nameSection, JButton Game, GameSectionViewImpl view);

	/**
	 * this method give the list of Section
	 * 
	 * @param section
	 */
	public void listSection(ArrayList<Section> section);

	/**
	 * this method use the pattern Observer
	 * 
	 * @param controller
	 */
	public void addObserver(GameSectionController controller);
	
	/**
	 * This method set the display of the view
	 * 
	 * @param section
	 */
	public void setPanel(ArrayList<Section> section);

}
