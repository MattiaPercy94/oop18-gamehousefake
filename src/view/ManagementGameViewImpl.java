package view;

import model.Game;
import controller.GameController;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

/**
 * @author Mattia Ferrari
 */

public class ManagementGameViewImpl extends JFrame implements
		ManagementGameView {

	private static final long serialVersionUID = 1L;
	private final JPanel mainPanel = new JPanel(new GridLayout(0, 7, 2, 2));
	private final JPanel componentPanel = new JPanel(new GridLayout(0, 7, 2, 2));
	private final JButton printGame = new JButton("Giochi");
	private final JLabel displayModify = new JLabel("Modifica");
	private final JLabel displayDelete = new JLabel("Elimina");
	private final JLabel displayPrice = new JLabel("Prezzo");
	private final JLabel displayTotalPrice = new JLabel("Prezzo totale");
	private final JLabel displayQuantity = new JLabel("Quantità");
	private final JLabel displaySupplier = new JLabel("Fornitore");
	GameController controller;

	public ManagementGameViewImpl() {

		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		this.setVisible(true);
		this.setResizable(false);
		this.setSize(700, 500);
		this.setBounds(300, 175, this.getWidth(), this.getHeight());

		this.setLayout(new BorderLayout());

		this.add(mainPanel, BorderLayout.NORTH);
		mainPanel.setBackground(new Color(192, 192, 192));
		componentPanel.setBackground(new Color(192, 192, 192));

		mainPanel.add(componentPanel);

		JScrollPane scroll = new JScrollPane(componentPanel,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		this.add(scroll);

		mainPanel.add(printGame);

		mainPanel.add(displayPrice);
		displayPrice.setFont(Utility.Utility.fontDisplay);
		mainPanel.add(displayQuantity);
		displayQuantity.setFont(Utility.Utility.fontDisplay);
		mainPanel.add(displayTotalPrice);
		displayTotalPrice.setFont(Utility.Utility.fontDisplay);
		mainPanel.add(displaySupplier);
		displaySupplier.setFont(Utility.Utility.fontDisplay);
		mainPanel.add(displayModify);
		displayModify.setFont(Utility.Utility.fontDisplay);
		mainPanel.add(displayDelete);
		displayDelete.setFont(Utility.Utility.fontDisplay);

		printGame.setVisible(false);

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quitModify();

			}

		});

	}

	public void addObserver(GameController controller) {

		this.controller = controller;
	}

	public void listGame(ArrayList<Game> game) {

		for (int i = 0; i < game.size(); i++) {

			JLabel nameGame = new JLabel(game.get(i).getName());
			JLabel priceGame = new JLabel("€" + game.get(i).getPrice());
			JLabel quantityGame = new JLabel(""
					+ game.get(i).getQuantity());
			JLabel totalPriceGame = new JLabel("€"
					+ game.get(i).getQuantity() * game.get(i).getPrice());
			JLabel  supplier = new JLabel(game.get(i).getSupplier());
			JButton deleteGame = new JButton("Elimina");
			JButton modifyGame = new JButton("Modifica");

			componentPanel.add(nameGame);
			componentPanel.add(priceGame);
			componentPanel.add(quantityGame);
			componentPanel.add(totalPriceGame);
			componentPanel.add(supplier);
			componentPanel.add(modifyGame);
			componentPanel.add(deleteGame);

			/*
			 * Qui per il metodo quit ho utilzzato il this nel metodo action
			 * listener
			 */
			actionEvent(deleteGame, game.get(i), modifyGame, this);
			validate();

		}

	}

	public void actionEvent(JButton deleteGame, Game game,
			JButton modifySection, ManagementGameViewImpl view) {

		modifySection.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				controller.modifyController(game, view);

			}

		});

		deleteGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				controller.deleteGame(game);

			}

		});

	}

	public void setPanel(ArrayList<Game> game) {

		componentPanel.removeAll();
		listGame(game);

	}

}
