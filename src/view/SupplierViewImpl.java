package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Utility.Utility;
import controller.SupplierController;
import model.Supplier;

public class SupplierViewImpl  extends JFrame  implements SupplierView {

	private static final long serialVersionUID = 1L;
	SupplierController controller;
	private final JPanel supplierPanel = new JPanel(new GridLayout(0, 3, 2, 2));
	private final JLabel displayDelete = new JLabel("Elimina");
	private final JLabel displayModify = new JLabel("Modifica");
	private final JLabel dislayName = new JLabel("Nome Fornitore");
	
	
	public SupplierViewImpl() {

		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		this.setVisible(true);
		this.setResizable(false);

		this.setSize(700, 500);
		this.setBounds(300, 175, this.getWidth(), this.getHeight());

		this.setLayout(new BorderLayout());

		this.add(supplierPanel, BorderLayout.NORTH);

		this.setLocationRelativeTo(null);
		supplierPanel.add(dislayName);
		supplierPanel.add(displayModify);
		supplierPanel.add(displayDelete);

		supplierPanel.setBackground(new Color(192, 192, 192));

		JScrollPane scroll = new JScrollPane(supplierPanel,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		this.add(scroll);

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quit();

			}

		});

	}

	@Override
	public void actionEvent(JButton deleteSupplier, Supplier supplier, JButton modifySupplier, JLabel nameSupplier,
			SupplierViewImpl view) {
		deleteSupplier.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				final int exit = JOptionPane.showConfirmDialog(view,
						Utility.QUESTIONDELETE2, "Cancellazione",
						JOptionPane.YES_NO_OPTION);
				if (exit == JOptionPane.YES_OPTION) {

					controller.deleteSupplier(supplier);

				}

			}

		});

		modifySupplier.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.setModifySupplierController(supplier, nameSupplier);

			}

		});
		
	}

	@Override
	public void listSupplier(ArrayList<Supplier> supplier) {
		for (int i = 0; i < supplier.size(); i++) {

			JLabel newLabel = new JLabel(supplier.get(i).getName());

			JButton modifyButton = new JButton("Modifica Fornitore");
			JButton deleteSupplier = new JButton("Elimina");

			supplierPanel.add(newLabel);
			supplierPanel.add(modifyButton);
			supplierPanel.add(deleteSupplier);

			actionEvent(deleteSupplier, supplier.get(i), modifyButton,newLabel,  this);

			validate();

		}

		
	}

	@Override
	public void addObserver(SupplierController controller) {
		this.controller = controller;

		listSupplier(controller.getListSupplierView());
		
	}

	@Override
	public void setPanel(ArrayList<Supplier> supplier) {
		supplierPanel.removeAll();
		supplierPanel.add(dislayName);
		supplierPanel.add(displayModify);
		supplierPanel.add(displayDelete);
		validate();
		listSupplier(supplier);
	}

}
