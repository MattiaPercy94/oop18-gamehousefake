package view;

import javax.swing.*;

import Utility.Utility;
import controller.MainController;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author Mattia Ferrari
 */

public class MainPanelImpl extends JFrame implements MainPanel {

	private static final long serialVersionUID = 12L;
	private JPanel mainPanel = new JPanel();
	private JPanel logInPanel = new JPanel();
	private final JLabel titleLabel = new JLabel("Gestionale Negozio di Videogiochi");
	private final JLabel titleLogIn = new JLabel("Benvenuti in GameHouse");
	private final JLabel displayLogIn = new JLabel("Inserire Credenziali :");
	private final JLabel displayUsername = new JLabel("Username:");
	private final JLabel selectOption = new JLabel("Selezionare una operazione");
	private final JTextField usernameText = new JTextField(20);
	private final JLabel displayPassword = new JLabel("Password:");
	private final JPasswordField passwordText = new JPasswordField(20);
	private final JButton submit = new JButton("Log-In");
	private final JButton insertGame = new JButton("Inserisci Gioco");
	private final JButton directGame = new JButton("Gestione Giochi/Sezioni");
	private final JButton insertSection = new JButton("Inserisci Sezione");
	private final JButton insertSupplier = new JButton("Inserisci Fornitore");
	private final JButton directSupplier = new JButton("Gestione Fornitori");
	
	MainController mainController;

	public MainPanelImpl() {

		super("Gestionale Negozio di Videogiochi");

		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		this.setVisible(true);
		this.setResizable(false);
		this.setSize(700, 500);
		this.setBounds(300, 175, this.getWidth(), this.getHeight());

		this.setBounds(300, 175, this.getWidth(), this.getHeight());

		mainPanel.setBackground(new Color(192, 192, 192));

		this.add(logInPanel);
		
		logInPanel.setBackground(new Color(192, 192, 192));

		SpringLayout spring = new SpringLayout();
		SpringLayout springLoginIn = new SpringLayout();

		mainPanel.setLayout(spring);
		logInPanel.setLayout(springLoginIn);

		Font fontDisplay = new Font("Arial", 15, 15);
		Font fontTitle = new Font("Arial", 20, 40);
		titleLogIn.setFont(fontTitle);
		logInPanel.add(titleLogIn);
		springLoginIn.putConstraint(SpringLayout.NORTH, titleLogIn, 60,SpringLayout.NORTH, this.getContentPane());
		springLoginIn.putConstraint(SpringLayout.WEST, titleLogIn, 120,SpringLayout.WEST, this.getContentPane());
		Font fontDisplayLogIn = new Font("Serif", 20, 20);
		displayLogIn.setFont(fontDisplayLogIn);
		logInPanel.add(displayLogIn);
		springLoginIn.putConstraint(SpringLayout.NORTH, displayLogIn, 120,SpringLayout.NORTH, this.getContentPane());
		springLoginIn.putConstraint(SpringLayout.WEST, displayLogIn, 230,SpringLayout.WEST, this.getContentPane());
		displayUsername.setFont(fontDisplay);
		logInPanel.add(displayUsername);
		springLoginIn.putConstraint(SpringLayout.NORTH, displayUsername, 180,SpringLayout.NORTH, this.getContentPane());
		springLoginIn.putConstraint(SpringLayout.WEST, displayUsername, 170,SpringLayout.WEST, this.getContentPane());
		logInPanel.add(usernameText);
		springLoginIn.putConstraint(SpringLayout.NORTH, usernameText, 180,SpringLayout.NORTH, this.getContentPane());
		springLoginIn.putConstraint(SpringLayout.WEST, usernameText, 270,SpringLayout.WEST, this.getContentPane());
		displayPassword.setFont(fontDisplay);
		logInPanel.add(displayPassword);
		springLoginIn.putConstraint(SpringLayout.NORTH, displayPassword, 210,SpringLayout.NORTH, this.getContentPane());
		springLoginIn.putConstraint(SpringLayout.WEST, displayPassword, 170,SpringLayout.WEST, this.getContentPane());
		logInPanel.add(passwordText);
		springLoginIn.putConstraint(SpringLayout.NORTH, passwordText, 210,SpringLayout.NORTH, this.getContentPane());
		springLoginIn.putConstraint(SpringLayout.WEST, passwordText, 270,SpringLayout.WEST, this.getContentPane());

		submit.setFont(fontDisplay);
		logInPanel.add(submit);
		springLoginIn.putConstraint(SpringLayout.NORTH, submit, 240,SpringLayout.NORTH, this.getContentPane());
		springLoginIn.putConstraint(SpringLayout.WEST, submit, 270,SpringLayout.WEST, this.getContentPane());
		
		
		mainPanel.add(titleLabel);
		titleLabel.setFont(fontTitle);
		spring.putConstraint(SpringLayout.NORTH, titleLabel, 60,SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, titleLabel,65,SpringLayout.WEST, this.getContentPane());
		selectOption.setFont(fontDisplayLogIn);
		mainPanel.add(selectOption);
		spring.putConstraint(SpringLayout.NORTH, selectOption, 160,SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, selectOption, 260,SpringLayout.WEST, this.getContentPane());
		insertGame.setFont(fontDisplay);
		
		mainPanel.add(insertSection);
		spring.putConstraint(SpringLayout.NORTH, insertSection, 210,SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, insertSection, 90,SpringLayout.WEST, this.getContentPane());
		insertSection.setFont(fontDisplay);
		
		mainPanel.add(insertSupplier);
		spring.putConstraint(SpringLayout.NORTH, insertSupplier, 260,SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, insertSupplier, 90,SpringLayout.WEST, this.getContentPane());
		insertSupplier.setFont(fontDisplay);
		
		directGame.setFont(fontDisplay);
		mainPanel.add(directGame);
		spring.putConstraint(SpringLayout.NORTH, directGame, 210,SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, directGame, 270,SpringLayout.WEST, this.getContentPane());
		
		directSupplier.setFont(fontDisplay);
		mainPanel.add(directSupplier);
		spring.putConstraint(SpringLayout.NORTH, directSupplier, 260,SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, directSupplier, 270,SpringLayout.WEST, this.getContentPane());
		
		mainPanel.add(insertGame);
		spring.putConstraint(SpringLayout.NORTH, insertGame, 210,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, insertGame, 490,
				SpringLayout.WEST, this.getContentPane());

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {
				quitExit();
			}

		});

		insertSection.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				mainController.insertSectionView();

			}

		});
		
		insertSupplier.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				mainController.insertSupplierView();

			}

		});

		insertGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				mainController.insertGameView();

			}

		});

		directGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				mainController.GameSection();

			}

		});
		
		directSupplier.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				mainController.Supplier();
				//JOptionPane.showMessageDialog(null, "AAA");

			}

		});
		
		submit.addActionListener(new ActionListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent arg0) {

				if (mainController.logIn(usernameText.getText(),
						passwordText.getText()) == true) {

					setMainPanel();
					logInPanel.setVisible(false);

					validate();

				} else {

					JOptionPane.showMessageDialog(MainPanelImpl.this,
							Utility.ERRORLOGIN);
					usernameText.setText("");
					passwordText.setText("");

				}

			}

		});

	}

	public void quitExit() {

		final int exit = JOptionPane.showConfirmDialog(this,
				Utility.QUESTIONLEAVE, "Uscita", JOptionPane.YES_NO_OPTION);
		if (exit == JOptionPane.YES_OPTION) {
			mainController.setFile();
			System.exit(0);
		}
	}

	public void addObserver(MainController mainController) {

		this.mainController = mainController;
	}

	public void setMainPanel() {

		this.getContentPane().add(mainPanel);
	}

}
