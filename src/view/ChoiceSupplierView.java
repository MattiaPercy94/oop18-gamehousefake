package view;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import controller.SupplierController;
import model.Supplier;


/**
 * @author Mattia Ferrari
 */

public interface ChoiceSupplierView {

	/**
	 * this method works with the component of JFrame
	 * 
	 * @param newButton
	 * @param suplier
	 * @param selectSupplier
	 * @param nameSupplier
	 */
	public void actionEvent(Supplier supplier, JButton selectSupplier,
			JLabel nameSupplier, ChoiceSupplierViewImpl view);
	/**
	 * this method give the list of Supplier
	 * 
	 * @param supplier
	 */
	public void listSupplier(ArrayList<Supplier> supplier);

	/**
	 * this method use the pattern Observer
	 * 
	 * @param controller
	 */
	public void addObserver(SupplierController controller);
	
	/**
	 * This method set the display of the view
	 * 
	 * @param supplier
	 */
	public void setPanel(ArrayList<Supplier> supplier);

}
