package view;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import controller.SupplierController;
import model.Supplier;


/**
 * @author Mattia Ferrari
 */

public interface SupplierView {

	/**
	 * this method works with the component of JFrame
	 * 
	 * @param newButton
	 * @param suplier
	 * @param modifySupplier
	 * @param nameSupplier
	 */
	public void actionEvent(JButton deleteSupplier, Supplier supplier, JButton modifySupplier,
			JLabel nameSupplier, SupplierViewImpl view);

	/**
	 * this method give the list of Supplier
	 * 
	 * @param supplier
	 */
	public void listSupplier(ArrayList<Supplier> supplier);

	/**
	 * this method use the pattern Observer
	 * 
	 * @param controller
	 */
	public void addObserver(SupplierController controller);
	
	/**
	 * This method set the display of the view
	 * 
	 * @param supplier
	 */
	public void setPanel(ArrayList<Supplier> supplier);

}
