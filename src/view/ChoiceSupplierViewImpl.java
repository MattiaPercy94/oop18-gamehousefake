package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import controller.SupplierController;
import model.Supplier;

public class ChoiceSupplierViewImpl extends JFrame implements ChoiceSupplierView{

	
	
	private static final long serialVersionUID = 1L;
	SupplierController controller;
	private final JPanel supplierPanel = new JPanel(new GridLayout(0, 2, 2, 2));
	private final JLabel displaySelect = new JLabel("Seleziona");
	private final JLabel dislayName = new JLabel("Nome Fornitore");


	public ChoiceSupplierViewImpl() {

		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		this.setVisible(true);
		this.setResizable(false);

		this.setSize(700, 500);
		this.setBounds(300, 175, this.getWidth(), this.getHeight());

		this.setLayout(new BorderLayout());

		this.add(supplierPanel, BorderLayout.NORTH);

		this.setLocationRelativeTo(null);
		supplierPanel.add(dislayName);
		supplierPanel.add(displaySelect);

		supplierPanel.setBackground(new Color(192, 192, 192));

		JScrollPane scroll = new JScrollPane(supplierPanel,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		this.add(scroll);

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quitChoice();

			}

		});

	}


	
	@Override
	public void actionEvent(Supplier supplier, JButton selectSupplier, JLabel nameSupplier, ChoiceSupplierViewImpl view) {
		{
			

			selectSupplier.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					controller.getChoiceTextField().setText(supplier.getName());
					controller.quitChoice();

				}

			});
			
		}
		
	}

	@Override
	public void listSupplier(ArrayList<Supplier> supplier) {
		for (int i = 0; i < supplier.size(); i++) {

			JLabel newLabel = new JLabel(supplier.get(i).getName());

			JButton selectButton = new JButton("Seleziona Fornitore");

			supplierPanel.add(newLabel);
			supplierPanel.add(selectButton);

			actionEvent(supplier.get(i), selectButton,newLabel,  this);

			validate();

		}
		
	}

	@Override
	public void addObserver(SupplierController controller) {
		this.controller = controller;

		listSupplier(controller.getListSupplierView());
		
	}

	@Override
	public void setPanel(ArrayList<Supplier> supplier) {
		supplierPanel.removeAll();
		supplierPanel.add(dislayName);
		supplierPanel.add(displaySelect);
		validate();
		listSupplier(supplier);
		
	}
	

}
