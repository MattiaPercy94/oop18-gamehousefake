package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import controller.SupplierController;

public class ModifySupplierViewImpl extends JFrame  implements ModifySupplierView {
	
	private static final long serialVersionUID = 1L;
	private final JPanel insertPanel = new JPanel();
	private final JLabel displayName = new JLabel("Nome :");
	private final JLabel displayTitle = new JLabel("Modifica fornitore");
	private JLabel displaynameSupplier;
	private final JTextField supplierName = new JTextField(20);
	private final JButton save = new JButton("Salva");
	private JLabel labelCheck = new JLabel();
	private int supplierCode;
	SupplierController controller;

	public ModifySupplierViewImpl() {

		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		this.setVisible(true);
		this.setResizable(false);

		this.setSize(700, 500);
		this.setBounds(300, 175, this.getWidth(), this.getHeight());

		SpringLayout spring = new SpringLayout();

		insertPanel.setBackground(new Color(192, 192, 192));

		this.add(insertPanel);
		insertPanel.setLayout(spring);

		insertPanel.add(displayTitle);
		displayTitle.setFont(Utility.Utility.fontTitle);
		spring.putConstraint(SpringLayout.NORTH, displayTitle, 10,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayTitle, 40,
				SpringLayout.WEST, this.getContentPane());
		insertPanel.add(displayName);
		displayName.setFont(Utility.Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayName, 100,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayName, 40,
				SpringLayout.WEST, this.getContentPane());
		insertPanel.add(supplierName);
		supplierName.setFont(Utility.Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, supplierName, 100,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, supplierName, 280,
				SpringLayout.WEST, this.getContentPane());
		insertPanel.add(labelCheck);
		labelCheck.setFont(Utility.Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, labelCheck, 220,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, labelCheck, 280,
				SpringLayout.WEST, this.getContentPane());
		insertPanel.add(save);
		save.setFont(Utility.Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, save, 250, SpringLayout.NORTH,
				this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, save, 400, SpringLayout.WEST,
				this.getContentPane());

		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String check = null;

				check = controller.modifySupplier(
						supplierName.getText(), supplierCode,displaynameSupplier
						);

				labelCheck.setText(check);

			}

		});

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quitModify();

			}

		});
	}

	
	@Override
	public void addObserver(SupplierController controller) {
		this.controller = controller;
		
	}

	@Override
	public void setData(String name, int code, JLabel nameSupplier) {
		supplierName.setText(name);
		supplierCode = code;
		displaynameSupplier = nameSupplier;
		
	}

}
