package view;

import java.util.ArrayList;

import javax.swing.JButton;


import controller.GameController;


import model.Game;


/**
 * @author Mattia Ferrari
 */

public interface ManagementGameView {

	/**
	 * this method use the pattern Observer
	 * 
	 * @param controller
	 */
	public void addObserver(GameController controller);

	/**
	 * this method give the list of game in the section
	 * 
	 * @param game
	 * @param section
	 */
	public void listGame(ArrayList<Game> game);

	/**
	 * this method works with the component of JFrame
	 * 
	 * @param deleteGame
	 * @param game
	 * @param modifySection
	 * @param nameGame
	 * @param view
	 * @param quantityGame
	 * @param priceGame
	 */
	public void actionEvent(JButton deleteGame, Game game, JButton modifySection,
			ManagementGameViewImpl view);
	
	
	
	/**
	 * 
	 * this method set the display of the view
	 * @param game
	 */
	public void setPanel(ArrayList<Game> game);

}
