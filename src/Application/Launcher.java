package Application;

import model.VideogameCenter;
import model.VideogameCenterImpl;
import controller.MainController;
import controller.MainControllerImpl;
import view.MainPanelImpl;

/**
 * @author Mattia Ferrari
 */

public class Launcher {

	public static void main(String[] args) {

		//Main Panel VIEW
		MainPanelImpl panel = new MainPanelImpl();
		
		//VideoGameCenter MODEL
		VideogameCenter videogameCenter = new VideogameCenterImpl();
		
		//MainController CONTROL
		MainController mainController = new MainControllerImpl(videogameCenter,	panel);
		
		
		//controller is also Observer of the VIEW (events management)
		panel.addObserver(mainController);

	}

}
