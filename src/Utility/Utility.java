package Utility;

import java.awt.Font;

public final class Utility {
	
	public static final String ERRORDATA = "Inserire Dati";
	public static final String ERRORCODEQUANTIY = "Codice e/o quantità max errate";
	public static final String ERRORCODE = "Codice già inserito";
	public static final String ERRORNAME = "Nome gia inserito";
	public static final String ERRORQUANTITY = "Quantità errata";
	public static final String ERRORCAPACITY = "Errore quantità, eccede capacità massima della sezione";
	public static final String ERRORLOGIN = "Credenziali errate";
	public static final String ERRORMODIFYQUANTIY = "Errore quantità inserita maggiore di quantità in sezione";
	public static final String ERRORFORMAT = "Elementi inseriti non giusti";
	
	public static final String CHECKQUANTITY = "Nome già inserito, ho modificato la quantità e/o prezzo";
	public static final String CHECKGAME = "Quantità e prezzo modificata";
	public static final String CHECKDELETEGAME = "Quantità reimpostata a 0, cancello il videogioco";
	
	public static final String SUCCESSMODIFY = "Modifica avvenuta con successo";
	public static final String SUCCESSINSERT = "Inserimento avvenuto con successo";
	
	
	public static final String QUESTIONLEAVE = "Stai per uscire, vuoi continuare?";
	public static final String QUESTIONDELETE = "Stai per cancellare questo reparto, vuoi continuare?";
	
	public static final String QUESTIONDELETE2 = "Stai per cancellare questo fornitore, vuoi continuare?";
	
	public static final String DELETE = "In cancellazione";
	
	
	public static final Font fontTitle = new Font("Serif",20,40);
	public static final Font fontDisplay = new Font("Times New Roman",15,15);
	
	
	private Utility(){
		
	};
}
