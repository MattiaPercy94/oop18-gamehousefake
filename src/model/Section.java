package model;

import java.util.ArrayList;


/**
 * @author Mattia Ferrari
 */

public interface Section {

	/**
	 * this method set a new name
	 * 
	 * @param name
	 */
	public void setName(String name);

	/**
	 * this method set a new maxGameSection
	 * 
	 * @param maxGameSection
	 */
	public void setMaxGameSection(int maxGameSection);

	/**
	 * this method return the code of section
	 * 
	 * @return
	 */
	public int getCodeSection();

	/**
	 * this method return the maxgame of Section
	 * 
	 * @return
	 */
	public int getMaxGameSection();

	/**
	 * this method return the name of Section
	 * 
	 * @return
	 */
	public String getName();

	/**
	 * this method return the amount of section
	 * 
	 * @return
	 */
	public int getsectionAmount();

	/**
	 * this method return the list of product in the Section
	 * 
	 * @return
	 */
	public int getListGameSize();

	/**
	 * this method insert a new game in the Section
	 * 
	 * @param game
	 */
	public void insertGame(Game game);

	/**
	 * this method return the list of game of Section
	 * 
	 * @return
	 */
	public ArrayList<Game> getListGame();
	
	/**
	 * this method delete a game
	 * 
	 * @return
	 */
	public void deleteGame(Game game);
	
	/**
	 * this method return the total quantity of game in the section
	 * 
	 * @return
	 */
	public int quantityTotal();
}
